
File name: lena256.jpg
Size: 62137 bytes
Resolution: 256x256
Red Variance: 2498.930145263672
Green Variance: 2873.8887634277344
Blue Variance: 1189.5587615966797
Least Variance: 2 (0 = Red, 1 = Green, 2 = Blue)
Number of runs: 30
==== ABC config ====
Cycles: 500
Colony Size: 40
Limit: 30