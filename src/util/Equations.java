package util;

public class Equations {
	public static double variance(int[] numbers) {
		double mean = mean(numbers), temp = 0;
		for (int i : numbers) {
			temp += Math.pow((mean - i), 2);
		}
		
		return temp/numbers.length;
	}
	
	public static int sum(int[] numbers) {
		int sum = 0;
		for (int i : numbers) {
			sum += i;
		}
		
		return sum;
	}
	
	public static int mean(int[] numbers) {
		int sum = sum(numbers);
		
		return sum/numbers.length;
	}
}
