package util;

import java.util.Random;

public class Randomizer {

	private static Random generator = new Random();
	
	public static int randomizeInt(int lowerBound, int upperBound) {
		return generator.nextInt(upperBound - lowerBound + 1) + lowerBound;
	}
	
	public static double randomizeZeroToOne() {
		return generator.nextDouble();
	}
	
	public static double randomizeDouble(double lowerBound, double upperBound) {
		return (generator.nextDouble() * (upperBound - lowerBound)) + lowerBound;
	}
	
}
