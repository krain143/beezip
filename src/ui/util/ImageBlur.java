package ui.util;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.util.Arrays;

public class ImageBlur {

	public static BufferedImage blur(BufferedImage image) {
		float[] data = new float[9];
		Arrays.fill(data, 0.11111f);
		Kernel kernel = new Kernel(3, 3, data);
		BufferedImageOp op = new ConvolveOp(kernel);
		return op.filter(image, null);
	}
	
}
