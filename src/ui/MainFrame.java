package ui;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JFrame;

public class MainFrame extends JFrame {
	public MainFrame() {
		initHomePanel();
		initFrame();
	}

	private void initFrame() {
		setMinimumSize(new Dimension(640, 480));
		setLocationByPlatform(true);
		setBounds(new Rectangle(0, 22, 640, 480));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Image Compress");
	}

	private void initHomePanel() {
		setContentPane(new HomePanel());
	}
}
