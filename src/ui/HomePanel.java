package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.TooManyListenersException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import ui.components.BGPanel;
import ui.components.ImagePanel;
import ui.components.RoundButton;
import ui.components.TiledBGPanel;

import java.awt.GridLayout;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.image.BufferedImage;

import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import lib.Controller;

public class HomePanel extends TiledBGPanel {

	private DropTarget dropTarget;
	private DropTargetHandler dropTargetHandler;

	private BGPanel mainPanel;
	private JPanel bottomPanel;
	private RoundButton openImageBtn;
	private JLabel dropLabel;
	private ImagePanel imagePanel;
	private ExperimentControlPanel experimentControlPanel;
	
	private BufferedImage splashBG, dropBG;
	private BufferedImage inputImage;
	private File inputFile;
	
	private Controller control = Controller.getInstance();

	public HomePanel(){
		setLayout(new BorderLayout());
		setBackground(Color.WHITE);
		initMainPanel();
		initBottomPanel();
		initImagePanel();
		initExperimentControlPanel();
	}

	private void initExperimentControlPanel() {
		experimentControlPanel = ExperimentControlPanel.getInstance();
	}

	private void initImagePanel() {
		imagePanel = ImagePanel.getInstance();
	}

	private void initMainPanel() {
		try {
			this.mainPanel = new BGPanel();
			this.mainPanel.setOpaque(false);
			//this.mainPanel.setLayout(null);
			add(this.mainPanel, BorderLayout.CENTER);

			this.splashBG = ImageIO.read(new File("images/bg.png"));
			this.dropBG = ImageIO.read(new File("images/drop.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initBottomPanel() {
		bottomPanel = new JPanel();
		bottomPanel.setOpaque(false);
		add(bottomPanel, BorderLayout.SOUTH);
		bottomPanel.setBorder(new EmptyBorder(0, 0, 20, 0));
		bottomPanel.setLayout(new BorderLayout(0, 0));

		openImageBtn = new RoundButton("OPEN IMAGE");
		JPanel buttonContainer = new JPanel();
		buttonContainer.setOpaque(false);
		buttonContainer.add(openImageBtn);
		openImageBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser("sample");
			    FileNameExtensionFilter filter = new FileNameExtensionFilter(
			        "JPG & GIF Images", "jpg", "gif", "png", "tiff");
			    chooser.setFileFilter(filter);
			    int returnVal = chooser.showOpenDialog(null);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			    	setInputFile(chooser.getSelectedFile());
			    }
			}
		});
		bottomPanel.add(buttonContainer, BorderLayout.CENTER);

		dropLabel = new JLabel("<html>or <strong>drop</strong> image anywhere on the panel</html>");
		dropLabel.setHorizontalAlignment(SwingConstants.CENTER);
		dropLabel.setFont(new Font("Open Sans", Font.PLAIN, 13));
		dropLabel.setForeground(Color.DARK_GRAY);
		bottomPanel.add(dropLabel, BorderLayout.SOUTH);
	}

	protected DropTarget getMyDropTarget() {
		if (dropTarget == null) {
			dropTarget = new DropTarget(this, DnDConstants.ACTION_COPY_OR_MOVE, null);
		}
		return dropTarget;
	}

	protected DropTargetHandler getDropTargetHandler() {
		if (dropTargetHandler == null) {
			dropTargetHandler = new DropTargetHandler();
		}
		return dropTargetHandler;
	}

	protected void showSplashBG() {
		this.mainPanel.setBG(this.splashBG);
	}

	protected void showDropBG() {
		this.mainPanel.setBG(this.dropBG);
	}

	@Override
	public void addNotify() {
		super.addNotify();
		try {
			getMyDropTarget().addDropTargetListener(getDropTargetHandler());
		} catch (TooManyListenersException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void removeNotify() {
		super.removeNotify();
		getMyDropTarget().removeDropTargetListener(getDropTargetHandler());
	}

	protected void importFiles(final List files) {
		Runnable run = new Runnable() {
			@Override
			public void run() {
				setInputFile((File) files.get(0));
			}
		};
		SwingUtilities.invokeLater(run);
	}

	protected void setInputFile(File file) {
		try {
			inputFile = file;
			inputImage = ImageIO.read(file);
			imagePanel.setInputFile(file);
			
			remove(mainPanel);
			add(imagePanel, BorderLayout.CENTER);
			
			remove(bottomPanel);
			add(experimentControlPanel, BorderLayout.SOUTH);
			
			updateUI();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected class DropTargetHandler implements DropTargetListener {

		protected void processDrag(DropTargetDragEvent dtde) {
			if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
				dtde.acceptDrag(DnDConstants.ACTION_COPY);
			} else {
				dtde.rejectDrag();
			}
		}

		@Override
		public void dragEnter(DropTargetDragEvent dtde) {
			processDrag(dtde);
			showDropBG();
		}

		@Override
		public void dragOver(DropTargetDragEvent dtde) {
			processDrag(dtde);
			showDropBG();
		}

		@Override
		public void dropActionChanged(DropTargetDragEvent dtde) {
		}

		@Override
		public void dragExit(DropTargetEvent dte) {
			showSplashBG();
		}

		@Override
		public void drop(DropTargetDropEvent dtde) {
			showSplashBG();

			Transferable transferable = dtde.getTransferable();
			if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
				dtde.acceptDrop(dtde.getDropAction());
				try {

					List transferData = (List) transferable.getTransferData(DataFlavor.javaFileListFlavor);
					if (transferData != null && transferData.size() > 0) {
						importFiles(transferData);
						dtde.dropComplete(true);
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				dtde.rejectDrop();
			}
		}
	}
}
