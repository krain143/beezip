package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import ui.components.ImagePanel;
import ui.components.RoundButton;
import javax.swing.SpinnerNumberModel;

import abc.ABCThread;
import javax.swing.JSeparator;
import java.awt.FlowLayout;
import java.awt.Insets;

public class ExperimentControlPanel extends JPanel {

	private static ExperimentControlPanel instance;
	
	private ABCThread abcThread;
	
	private JSpinner runs, cycles, colony, limit;
	private RoundButton start, stop;

	private ImagePanel imgPanel;
	private JPanel panel, buttonContainer;
	private JSeparator separator;

	private ExperimentControlPanel() {
		initPanel();
		initComponents();
		
		imgPanel = ImagePanel.getInstance();
		abcThread = ABCThread.getInstance();
	}

	private void initPanel() {
		//setOpaque(false);
		//setBackground(new Color(240, 240, 240));
		//setBackground(new Color(0, 0, 0, 0.5f));
		setBackground(Color.white);
		setBorder(new EmptyBorder(0, 0, 20, 0));
		setLayout(new BorderLayout());
	}

	private void initComponents() {
		runs = new JSpinner();
		runs.setModel(new SpinnerNumberModel(1, 1, 100, 1));
		runs.setFont(new Font("Open Sans", Font.BOLD, 18));
		
		JPanel runsPanel = new JPanel(new BorderLayout());
		runsPanel.setOpaque(false);
		JLabel runsLabel = new JLabel("RUNS:");
		runsLabel.setForeground(Color.GRAY);
		runsLabel.setFont(new Font("Open Sans", Font.BOLD, 12));
		runsPanel.add(runsLabel, BorderLayout.NORTH);
		runsPanel.add(runs, BorderLayout.CENTER);
		
		cycles = new JSpinner();
		cycles.setModel(new SpinnerNumberModel(500, 250, 5000, 50));
		cycles.setFont(new Font("Open Sans", Font.BOLD, 18));
		
		JPanel cyclesPanel = new JPanel(new BorderLayout());
		cyclesPanel.setOpaque(false);
		JLabel cyclesLabel = new JLabel("CYCLES:");
		cyclesLabel.setForeground(Color.GRAY);
		cyclesLabel.setFont(new Font("Open Sans", Font.BOLD, 12));
		cyclesPanel.add(cyclesLabel, BorderLayout.NORTH);
		cyclesPanel.add(cycles, BorderLayout.CENTER);
		
		colony = new JSpinner();
		colony.setModel(new SpinnerNumberModel(20, 10, 100, 5));
		colony.setFont(new Font("Open Sans", Font.BOLD, 18));
		
		JPanel colonyPanel = new JPanel(new BorderLayout());
		colonyPanel.setOpaque(false);
		JLabel colonyLabel = new JLabel("COLONY SIZE:");
		colonyLabel.setForeground(Color.GRAY);
		colonyLabel.setFont(new Font("Open Sans", Font.BOLD, 12));
		colonyPanel.add(colonyLabel, BorderLayout.NORTH);
		colonyPanel.add(colony, BorderLayout.CENTER);
		
		limit = new JSpinner();
		limit.setOpaque(false);
		limit.setBackground(Color.LIGHT_GRAY);
		limit.setModel(new SpinnerNumberModel(30, 10, 100, 10));
		limit.setFont(new Font("Open Sans", Font.BOLD, 18));
		
		JPanel limitPanel = new JPanel(new BorderLayout());
		limitPanel.setOpaque(false);
		JLabel limitLabel = new JLabel("LIMIT:");
		limitLabel.setForeground(Color.GRAY);
		limitLabel.setFont(new Font("Open Sans", Font.BOLD, 12));
		limitPanel.add(limitLabel, BorderLayout.NORTH);
		limitPanel.add(limit, BorderLayout.CENTER);
		
		JPanel spinnerContainer = new JPanel(new GridLayout(1, 4, 15, 0));
		spinnerContainer.setBorder(new EmptyBorder(0, 20, 0, 0));
		spinnerContainer.setOpaque(false);
		spinnerContainer.add(runsPanel);
		spinnerContainer.add(cyclesPanel);
		spinnerContainer.add(colonyPanel);
		spinnerContainer.add(limitPanel);
		add(spinnerContainer, BorderLayout.CENTER);
		
		start = new RoundButton("START");
		start.setPreferredSize(new Dimension(105, 39));
		//start.setBorder(new EmptyBorder(6, 25, 6, 25));
		start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//start.setEnabled(false);
				buttonContainer.remove(start);
				buttonContainer.add(stop);
				abcThread.startThread(
					imgPanel.getInputImage(),
					((Integer) runs.getValue()).intValue(),
					((Integer) cycles.getValue()).intValue(),
					((Integer) colony.getValue()).intValue(),
					((Integer) limit.getValue()).intValue()
				);
			}
		});
		
		stop = new RoundButton("CANCEL");
		stop.setPreferredSize(new Dimension(105, 39));
		//stop.setBorder(new EmptyBorder(6, 0, 6, 0));
		stop.setBackground(new Color(199, 29, 57));
		stop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				abcThread.stopThread();
				buttonContainer.remove(stop);
				buttonContainer.add(start);
			}
		});
		
		buttonContainer = new JPanel();
		FlowLayout flowLayout = (FlowLayout) buttonContainer.getLayout();
		flowLayout.setHgap(0);
		flowLayout.setVgap(0);
		buttonContainer.setOpaque(false);
		buttonContainer.setBorder(new EmptyBorder(17, 20, 0, 20));
		buttonContainer.add(start);
		add(buttonContainer, BorderLayout.EAST);
		
		panel = new JPanel();
		panel.setBorder(new EmptyBorder(0, 0, 20, 0));
		panel.setOpaque(false);
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		separator = new JSeparator();
		separator.setPreferredSize(new Dimension(0, 1));
		separator.setBackground(new Color(0, 0, 0));
		separator.setForeground(new Color(210, 210, 210));
		panel.add(separator, BorderLayout.NORTH);
	}

	public static ExperimentControlPanel getInstance() {
		return instance == null ? (instance = new ExperimentControlPanel()) : instance;
	}

	public void finishExperiment() {
		buttonContainer.remove(stop);
		buttonContainer.add(start);
	}

}
