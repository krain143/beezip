package ui.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ui.util.ColorFunctions;

public class TransparentTextField extends JTextField {

	private Color bg = new Color(0, 0, 0, 100);

	public TransparentTextField(String text) {
		super(text);
		initAppearance();
		initFocusEffect();
	}

	private void initAppearance() {
		setOpaque(false);
		setForeground(Color.WHITE);
		setBorder(new EmptyBorder(5, 10, 5, 10));
		setBackground(new Color(0, 0, 0, 100));
		setCaretColor(new Color(255, 255, 255, 255));
		setSelectionColor(Color.DARK_GRAY);
		setSelectedTextColor(Color.WHITE);
		setFont(new Font("Open Sans", Font.PLAIN, 13));
	}

	private void initFocusEffect() {
		addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {
				setBackground(bg);
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				setBackground(ColorFunctions.alpha(bg, 150));
			}
		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(getBackground());
		g2.fillRoundRect(0,0, getWidth(),getHeight(), 4, 4);
		super.paintComponent(g);
	}
}
