package ui.components;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class TiledBGPanel extends JPanel {

	private BufferedImage tileBG;
	private int tileWidth, tileHeight;

	public TiledBGPanel(BufferedImage tileBG) {
		this.tileBG = tileBG;
		tileWidth = tileBG.getWidth();
		tileHeight = tileBG.getHeight();
	}

	public TiledBGPanel() {
		try {
			tileBG = ImageIO.read(new File("images/cream_pixels.png"));
			tileWidth = tileBG.getWidth();
			tileHeight = tileBG.getHeight();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int width = getWidth();
		int height = getHeight();
		int imageW = this.tileWidth;
		int imageH = this.tileHeight;

		// Tile the image to fill our area.  
		for (int x = 0; x < width; x += imageW) {
			for (int y = 0; y < height; y += imageH) {  
				g.drawImage(this.tileBG, x, y, this);  
			}  
		}  
	}

}
