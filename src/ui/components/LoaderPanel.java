package ui.components;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.BorderLayout;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class LoaderPanel extends JPanel {
	public LoaderPanel() throws IOException {
		setBackground(Color.WHITE);
		setLayout(new BorderLayout(0, 0));
		
		JLabel label = new JLabel("");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setIcon(new ImageIcon("/Users/kurtobispo/Downloads/freebie-archive-1394087512/128x128/Preloader_4/Preloader_4.gif"));
		add(label, BorderLayout.CENTER);
		initLoaderLabel();
	}

	private void initLoaderLabel() {
		
	}
}
