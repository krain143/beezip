package ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import lib.ValueStore;

import ui.util.ImageBlur;

public class ImagePanel extends JPanel {
	private static ImagePanel instance;
	
	private JLabel inputImageLabel, outputImageLabel, overlay, fileNameLabel;
	private BufferedImage inputImage;
	private File inputFile;
	private RoundButton openImage;

	private ImagePanel() {
		//setBorder(new LineBorder(Color.LIGHT_GRAY));
		setBackground(Color.white);
		setOpaque(false);
		setLayout(null);
		
		inputImageLabel = new JLabel();
		inputImageLabel.setLayout(new BorderLayout());
		inputImageLabel.setBackground(Color.WHITE);
		inputImageLabel.setOpaque(true);
		inputImageLabel.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192, 192)), new EmptyBorder(5, 5, 5, 5)));
		overlay = new JLabel();
		overlay.setLayout(new BorderLayout());
		overlay.setBackground(new Color(0, 0, 0, 0.75f));
		overlay.setOpaque(true);
		overlay.setVisible(false);
		
		fileNameLabel = new JLabel();
		fileNameLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		fileNameLabel.setFont(new Font("Open Sans", Font.PLAIN, 13));
		fileNameLabel.setForeground(Color.white);
		fileNameLabel.setHorizontalAlignment(JLabel.CENTER);
		overlay.add(fileNameLabel, BorderLayout.CENTER);
		
		openImage = new RoundButton("CHANGE IMAGE", 5, new Color(64, 200, 100));
		openImage.setFontSize(11);
		openImage.setBorder(new EmptyBorder(5, 13, 5, 13));
		openImage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser("sample");
			    FileNameExtensionFilter filter = new FileNameExtensionFilter(
			        "JPG & GIF Images", "jpg", "gif", "png", "tiff");
			    chooser.setFileFilter(filter);
			    int returnVal = chooser.showOpenDialog(null);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			    	setInputFile(chooser.getSelectedFile());
			    }
			}
		});
		JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		bottomPanel.setBorder(new EmptyBorder(0, 0, 20, 0));
		bottomPanel.setOpaque(false);
		bottomPanel.add(openImage);
		overlay.add(bottomPanel, BorderLayout.SOUTH);
		
		inputImageLabel.add(overlay, BorderLayout.CENTER);
		add(inputImageLabel);
		inputImageLabel.updateUI();
		
		inputImageLabel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}

			@Override
			public void mouseClicked(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {
				if (!inputImageLabel.getVisibleRect().contains(e.getPoint())) {
					overlay.setVisible(false);
					inputImageLabel.updateUI();
				}
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				if (!ValueStore.isExperimenting) {
					overlay.setVisible(true);
					inputImageLabel.updateUI();
				}
			}
		});

		outputImageLabel = new JLabel();
		outputImageLabel.setBackground(Color.WHITE);
		outputImageLabel.setOpaque(true);
		outputImageLabel.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192, 192)), new EmptyBorder(5, 5, 5, 5)));
		add(outputImageLabel);
	}
	
	public static ImagePanel getInstance() {
		return instance == null ? (instance = new ImagePanel()) : instance;
	}
	
	public void setInputFile(File input) {
		inputFile = input;
		fileNameLabel.setText(inputFile.getName());
		fileNameLabel.setToolTipText(inputFile.getName());
		try {
			inputImage = ImageIO.read(input);
			inputImageLabel.setIcon(new ImageIcon(inputImage));
			outputImageLabel.setIcon(null);
			updateUI();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public File getInputFile() {
		return inputFile;
	}
	
	public void setOutputImage(BufferedImage image) {
		outputImageLabel.setIcon(new ImageIcon(image));
		updateUI();
	}
	
	public BufferedImage getInputImage() {
		return inputImage;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		if (inputImage != null) {
			overlay.setVisible(false);
			inputImageLabel.setBounds((getWidth() - 12) / 2 - inputImage.getWidth() - 20, (getHeight() - inputImage.getHeight() - 12) / 2, inputImage.getWidth() + 12, inputImage.getHeight() + 12);
			outputImageLabel.setBounds((getWidth() - 12) / 2 + 20, (getHeight() - inputImage.getHeight() - 12) / 2, inputImage.getWidth() + 12, inputImage.getHeight() + 12);
			//overlay.setVisible(true);
		}
		super.paintComponent(g);
		overlay.updateUI();
	}
}
