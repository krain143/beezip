package ui.components;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class BGPanel extends JPanel {
	private BufferedImage bg;

	public BGPanel(BufferedImage bg) {
		this.bg = bg;
	}

	public BGPanel() throws IOException {
		this(ImageIO.read(new File("images/bg.png")));
	}

	public void setBG(BufferedImage bg) {
		this.bg = bg;
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (this.bg == null) return;
		int x = getWidth() / 2 - bg.getWidth() / 2;
		int y= getHeight() / 2 - bg.getHeight() / 2;
		g.drawImage(bg, x, y, bg.getWidth(), bg.getHeight(), this);
	}
}
