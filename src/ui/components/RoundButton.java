package ui.components;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;

import javax.swing.JButton;
import javax.swing.border.EmptyBorder;

import ui.util.ColorFunctions;

public class RoundButton extends JButton {
	private int radius;
	private Color bg;

	public RoundButton(String text, int radius, Color bg) {
		super(text);
		this.radius = radius;
		this.bg = bg;
		initAppearance();
		initHoverEffect();
	}

	public RoundButton(String text) {
		this(text, 7, new Color(64, 153, 255));
	}

	private void initHoverEffect() {
		addMouseListener(new MouseListener() {
			@Override
			public void mouseEntered(MouseEvent e) {
				setBackground(ColorFunctions.brightness(bg, 0.90));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setBackground(bg);
			}

			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
	}

	private void initAppearance() {
		setContentAreaFilled(true);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		setBorder(new EmptyBorder(10, 25, 10, 25));
		setBackground(this.bg);
		setFont(new Font("Open Sans", Font.BOLD, 18));
		setForeground(new Color(255, 255, 255));
		setRolloverEnabled(false);
	}
	
	public void setFontSize(int fontSize) {
		setFont(getFont().deriveFont(Font.BOLD, fontSize));
	}
	
	public void setBackground(Color color) {
		bg = color;
		super.setBackground(color);
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(getBackground());
		g2.fillRoundRect(0, 0, getWidth(), getHeight(), this.radius, this.radius);

		Rectangle2D textBounds = g2.getFontMetrics().getStringBounds(getText(), g2);
		float x = getWidth() / 2 - (float) textBounds.getWidth() / 2; 
		float y = (getHeight() / 2) - (float) textBounds.getHeight() / 2;
		g2.setColor(getForeground());
		g2.drawString(getText(), x, y + g2.getFontMetrics().getFont().getSize());

		g2.dispose();
	}
}
