package lib;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import util.Equations;

public class ImageModel {

	public static final int RED = 0, GREEN = 1, BLUE = 2;  
	
	private BufferedImage image = null;
	private int width, height;
	private int[] alpha, min, max, rgb;
	private int[][] colors, modifiedColors;
	private double[] variance;
	private int leastVariance = -1;
	
	public ImageModel(File img) throws IOException{
		this(ImageIO.read(img));
	}
	
	public ImageModel(BufferedImage img) {
		this.image = img;
		initProperties();
	}
	
	public int[] getRedColors() {
		return this.colors[RED];
	}
	
	public int[] getGreenColors() {
		return this.colors[GREEN];
	}
	
	public int[] getBlueColors() {
		return this.colors[BLUE];
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public int getLeastVariance() {
		return this.leastVariance;
	}
	
	public double[] getVariance() {
		return this.variance;
	}
	
	public int[] getLeastVarianceComponent() {
		return this.colors[this.leastVariance];
	}
	
	public int[] getMin() {
		return this.min;
	}
	
	public int[] getMax() {
		return this.max;
	}
	
	public double getPSNR(int[] newSolution) {
		return 10 * Math.log10(255 * 255 / getMSE(newSolution));
	}
	
	public double getMSE(int[] newSolution) {
		double mse = 0;
		for (int i = 0, k = 0; i < this.height; i += 2) {
			for (int j = 0; j < this.width; j += 2, k++) {
				int pos = i * width + j;
				mse += Math.pow(colors[this.leastVariance][pos] - newSolution[k], 2);
				mse += Math.pow(colors[this.leastVariance][pos + 1] - newSolution[k], 2);
				mse += Math.pow(colors[this.leastVariance][pos + width] - newSolution[k], 2);
				mse += Math.pow(colors[this.leastVariance][pos + width + 1] - newSolution[k], 2);
			}
		}
		return mse / (3 * width * height);
	}
	
	public BufferedImage getNewImage(int[] newColorComponent) {
		BufferedImage img = getImageClone();
		int width = this.width / 2 * 2;
		for (int i = 0, k = 0; i < this.height; i += 2) {
			for (int j = 0; j < this.width; j += 2, k++) {
				int pos = i * width + j;
				modifiedColors[this.leastVariance][pos] = (int) newColorComponent[k];
				modifiedColors[this.leastVariance][pos + 1] = (int) newColorComponent[k];
				modifiedColors[this.leastVariance][pos + width] = (int) newColorComponent[k];
				modifiedColors[this.leastVariance][pos + width + 1] = (int) newColorComponent[k];

				img.setRGB(j, i, colorToRGB(modifiedColors[RED][pos], modifiedColors[GREEN][pos], modifiedColors[BLUE][pos]));
				img.setRGB(j + 1, i, colorToRGB(modifiedColors[RED][pos + 1], modifiedColors[GREEN][pos + 1], modifiedColors[BLUE][pos + 1]));
				img.setRGB(j, i + 1, colorToRGB(modifiedColors[RED][pos + width], modifiedColors[GREEN][pos + width], modifiedColors[BLUE][pos + width]));
				img.setRGB(j + 1, i + 1, colorToRGB(modifiedColors[RED][pos + width + 1], modifiedColors[GREEN][pos + width + 1], modifiedColors[BLUE][pos + width + 1]));
			}
		}
		return img;
	}
	
	private BufferedImage getImageClone() {
		BufferedImage clone = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		for (int i = 0; i < image.getHeight(); i++) {
			for (int j = 0; j < image.getWidth(); j++) {
				clone.setRGB(j, i, image.getRGB(j, i));
			}
		}
		return clone;
	}

	public File writeImage(int[] newColorComponent, String fileName) throws IOException {
		BufferedImage img = getImageClone();
		int width = this.width / 2 * 2;
		for (int i = 0, k = 0; i < this.height; i += 2) {
			for (int j = 0; j < this.width; j += 2, k++) {
				int pos = i * width + j;
//				System.out.print(newColorComponent[k] + ", ");
				modifiedColors[this.leastVariance][pos] = (int) newColorComponent[k];
				modifiedColors[this.leastVariance][pos + 1] = (int) newColorComponent[k];
				modifiedColors[this.leastVariance][pos + width] = (int) newColorComponent[k];
				modifiedColors[this.leastVariance][pos + width + 1] = (int) newColorComponent[k];

				img.setRGB(j, i, colorToRGB(modifiedColors[RED][pos], modifiedColors[GREEN][pos], modifiedColors[BLUE][pos]));
				img.setRGB(j + 1, i, colorToRGB(modifiedColors[RED][pos + 1], modifiedColors[GREEN][pos + 1], modifiedColors[BLUE][pos + 1]));
				img.setRGB(j, i + 1, colorToRGB(modifiedColors[RED][pos + width], modifiedColors[GREEN][pos + width], modifiedColors[BLUE][pos + width]));
				img.setRGB(j + 1, i + 1, colorToRGB(modifiedColors[RED][pos + width + 1], modifiedColors[GREEN][pos + width + 1], modifiedColors[BLUE][pos + width + 1]));
			}
		}
		File file = new File(fileName + ".jpg");
		ImageIO.write(img, "jpg", file);
		
		return file;
	}
	
	private void initProperties() {
		initDimensions();
		initRGB();
		initVariance();
		initLimits();
	}
	
	private void initDimensions() {
		this.width = this.image.getWidth();
		this.height = this.image.getHeight();
		
		int length = this.width * this.height;
		this.colors = new int[3][length];
		this.modifiedColors = new int[3][length];
		this.alpha = new int[length];
		this.rgb = new int[length];
		
		this.min = new int[length / 4];
		this.max = new int[length / 4];
		
		System.out.println("WIDTH: " + this.width);
		System.out.println("HEIGHT: " + this.height);
	}
	
	private void initRGB() {
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				Color color = new Color(image.getRGB(col, row));
				this.alpha[row * this.width + col] = color.getAlpha();
				this.rgb[row * this.width + col] = color.getRGB();
				
				this.colors[RED][row * this.width + col] = color.getRed();
				this.modifiedColors[RED][row * this.width + col] = color.getRed();
				//this.min[RED] = this.colors[RED][row * this.width + col] < this.min[RED] ? color.getRed() : this.min[RED];
				//this.max[RED] = this.colors[RED][row * this.width + col] > this.max[RED] ? color.getRed() : this.max[RED];
				
				this.colors[GREEN][row * this.width + col] = color.getGreen();
				this.modifiedColors[GREEN][row * this.width + col] = color.getGreen();
				//this.min[GREEN] = this.colors[GREEN][row * this.width + col] < this.min[GREEN] ? color.getRed() : this.min[GREEN];
				//this.max[GREEN] = this.colors[GREEN][row * this.width + col] > this.max[GREEN] ? color.getRed() : this.max[GREEN];
				
				this.colors[BLUE][row * this.width + col] = color.getBlue();
				this.modifiedColors[BLUE][row * this.width + col] = color.getBlue();
				//this.min[BLUE] = this.colors[BLUE][row * this.width + col] < this.min[BLUE] ? color.getRed() : this.min[BLUE];
				//this.max[BLUE] = this.colors[BLUE][row * this.width + col] > this.max[BLUE] ? color.getRed() : this.max[BLUE];
			}
		}
	}
	
	private void initVariance() {
		variance = new double[3];
		double redVariance = Equations.variance(this.colors[RED]);
		double greenVariance = Equations.variance(this.colors[GREEN]);
		double blueVariance = Equations.variance(this.colors[BLUE]);
		
		variance[this.RED] = redVariance;
		variance[this.GREEN] = greenVariance;
		variance[this.BLUE] = blueVariance;

		if ((redVariance < greenVariance && greenVariance < blueVariance) || (redVariance < blueVariance && blueVariance < greenVariance)) {
			this.leastVariance = RED;
		}
		else if ((greenVariance < blueVariance && blueVariance < redVariance) || (greenVariance < redVariance && redVariance < blueVariance)) {
			this.leastVariance = GREEN;
		}
		else {
			this.leastVariance = BLUE;
		}
		
		System.out.println("RED VARIANCE: " + redVariance);
		System.out.println("GREEN VARIANCE: " + greenVariance);
		System.out.println("BLUE VARIANCE: " + blueVariance);
		System.out.println("LEAST VARIANCE: " + this.leastVariance);
	}
	
	private void initLimits() {
		for (int i = 0; i < ((width * height) / 4); i++) {
			int min = 255, max = 0;
			for (int j = (i / (width / 2)) * 2; j < ((i / (width / 2)) * 2) + 2; j++) {
				for (int k = (i % (height / 2)) * 2; k < ((i % (height / 2)) * 2) + 2; k++) {
					min = colors[leastVariance][j * width + k] < min ? colors[leastVariance][j * width + k] : min;
					max = colors[leastVariance][j * width + k] > max ? colors[leastVariance][j * width + k] : max;
				}
			}
			this.min[i] = min;
			this.max[i] = max;
		}
	}
	
	private int colorToRGB(int red, int green, int blue) {
		//return new Color(red, green, blue).getRGB();
		int newPixel = 0;
        newPixel += red; newPixel = newPixel << 8;
        newPixel += green; newPixel = newPixel << 8;
        newPixel += blue;
 
        return newPixel;
    }
}
