package lib;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class Controller {
	private static Controller instance;
	
	private File inputFile;
	private BufferedImage inputImage;
	
	private Controller() {}
	
	public static Controller getInstance() {
		return instance == null ? (instance = new Controller()) : instance;
	}
	
	public void setFile(File inputFile) {
		this.inputFile = inputFile;
		try {
			this.inputImage = ImageIO.read(inputFile);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public File getIputFile() {
		return inputFile;
	}
	
	public BufferedImage getInputImage() {
		return inputImage;
	}
}
