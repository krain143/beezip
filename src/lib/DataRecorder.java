package lib;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class DataRecorder {
	
	BufferedWriter bw;
	String filename;
	ArrayList<String> lines = new ArrayList<String>();
	
	public DataRecorder(String filename) {
		this.filename = filename;
		try {
			
		}catch(Exception e){}
	}
	
	public void addRow(String data) {
		lines.add(data);
	}
	public void writeFile() {
		try {
			bw = new BufferedWriter(new FileWriter(filename));
			for (String line : lines) {
				bw.newLine();
				bw.write(line);
			}
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
