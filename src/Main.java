import java.awt.EventQueue;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import ui.MainFrame;

import abc.ArtificialBeeColony;
import lib.ImageModel;


public class Main {
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*public static void main(String[] args) {
		JFileChooser chooser = new JFileChooser("C:\\Users\\Kurt Jerome\\Downloads\\sp");
	    FileNameExtensionFilter filter = new FileNameExtensionFilter(
	        "JPG & GIF Images", "jpg", "gif", "png", "tiff");
	    chooser.setFileFilter(filter);
	    int returnVal = chooser.showOpenDialog(null);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	       System.out.println("You chose to open this file: " +
	            chooser.getSelectedFile().getName());
	       ImageProperties imgProp = null;
	       try {
	    	   imgProp = new ImageProperties(chooser.getSelectedFile());
	       } catch (Exception e) {e.printStackTrace();}
	       
	       ArtificialBeeColony abc = new ArtificialBeeColony(imgProp, 15, 30);
	       for (int iter = 0; iter < 500; iter++) {
	    	   System.out.println(iter + ": " + -abc.getBestSolutionValue());
	    	   abc.sendEmployedBees();
	    	   abc.sendOnlookerBees();
	    	   abc.sendScoutBees();
	       }
	       
	       int[] optimal = abc.getBestSolution();
	       System.out.println("PSNR: " + -abc.getBestSolutionValue());
	       try {
	    	   imgProp.writeImage(optimal, "optimal");
	       } catch (IOException e) {
	    	   e.printStackTrace();
	       }
	    }
	}*/
}
