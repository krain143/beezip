package abc;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;

import ui.ExperimentControlPanel;
import ui.components.ImagePanel;

import lib.DataRecorder;
import lib.ImageModel;
import lib.ValueStore;

public class ABCThread {
	
	private static ABCThread instance;

	private boolean isStopped;

	private ArtificialBeeColony abc;
	private ImageModel imgModel;
	
	private ImagePanel imgPanel;
	private ExperimentControlPanel expPanel;
	
	private ABCThread() {
		imgPanel = ImagePanel.getInstance();
		//expPanel = ExperimentControlPanel.getInstance();
	}
	
	public static ABCThread getInstance() {
		return instance == null ? (instance = new ABCThread()) : instance;
	}

	public void startThread(final BufferedImage img, final int runs, final int cycles, final int colony, final int limit) {
		imgModel = new ImageModel(img);
		ValueStore.isExperimenting = true;
		isStopped = false;
		new Thread() {
			public void run() {
				String rootDir = "OUTPUT/mandrill-colored/";
				//compression data
				DataRecorder cd = new DataRecorder(rootDir+"data.csv");
				DataRecorder metas = new DataRecorder(rootDir+"metas.txt");
				cd.addRow("run(s), MSE, PSNR (dB), CR%, file size (bytes)");
				File inputFile = imgPanel.getInputFile();
				long original_size = inputFile.length();
				for (int i = 0; i < runs && !isStopped; i++) {
					abc = new ArtificialBeeColony(imgModel, colony, limit);
					for (int j = 0; j < cycles && !isStopped; j++) {
						abc.sendEmployedBees();
						abc.sendOnlookerBees();
						abc.sendScoutBees();
						imgPanel.setOutputImage(imgModel.getNewImage(abc.getBestSolution()));
//						System.out.println(abc.getBestSolutionValue());
					}
					if (ValueStore.isExperimenting) {
						System.out.println("Run(s): "+(i+1));
						int[] bestSol = abc.getBestSolution();
						double mse = imgModel.getMSE(bestSol);
						double psnr = abc.getBestSolutionValue();
						try {
							File imageFile = imgModel.writeImage(bestSol, rootDir+"compressed images/"+(i + 1));
							long file_size = imageFile.length();
							double cr = file_size/(double)original_size;
							cd.addRow((i + 1)+", "+mse+", "+psnr+", "+cr+", "+file_size);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
				if (ValueStore.isExperimenting) {
					cd.writeFile();
					metas.addRow("File name: "+inputFile.getName());
					metas.addRow("Size: "+original_size+" bytes");
					metas.addRow("Resolution: "+imgModel.getHeight()+"x"+imgModel.getWidth());
					double[] variance = imgModel.getVariance();
					metas.addRow("Red Variance: "+variance[imgModel.RED]);
					metas.addRow("Green Variance: "+variance[imgModel.GREEN]);
					metas.addRow("Blue Variance: "+variance[imgModel.BLUE]);
					metas.addRow("Least Variance: "+imgModel.getLeastVariance()+" (0 = Red, 1 = Green, 2 = Blue)");
					metas.addRow("Number of runs: "+runs);
					metas.addRow("==== ABC config ====");
					metas.addRow("Cycles: "+cycles);
					metas.addRow("Colony Size: "+colony);
					metas.addRow("Limit: "+limit);
					metas.writeFile();
				}
				//expPanel.finishExperiment();
				ValueStore.isExperimenting = false;
			}
		}.start();
	}
	
	public void stopThread() {
		isStopped = true;
	}

}
