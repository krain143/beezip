package abc;
import java.util.Arrays;

import lib.ImageModel;
import util.Randomizer;


public class ArtificialBeeColony {
	private int foodNumber;
	private int parameterSize;
	private int[] upperBound, lowerBound;
	private int limit;

	private int foods[][];
	private int trial[];
	private double probs[];
	private double ofValues[];
	private double fitnessValues[];
	//	default value 1 for both modification rate and scaling factor would simulate the basic ABC
	//	modification rate; value must be [0, 1]
	private double mr = 1.0;
	//scaling factor;
	private double sf = 1.0;

	private double bestSolutionValue, bestSolutionFitnessValue;
	private int bestSolution[];
	private int bestSolutionIndex;

	private ImageModel img;

	public ArtificialBeeColony(ImageModel img, int colonySize, int limit) {
		this.img = img;
		this.foodNumber = colonySize / 2;
		this.parameterSize = (img.getWidth() * img.getHeight()) / 4;
		this.lowerBound = img.getMin();
		this.upperBound = img.getMax();
		this.limit = limit;

		bestSolutionFitnessValue = 0;
		foods = new int[foodNumber][parameterSize];
		ofValues = new double[foodNumber];
		fitnessValues = new double[foodNumber];
		probs = new double[foodNumber];
		trial = new int[foodNumber];
		bestSolution = new int[parameterSize];

		initializeFoodSources();
		memorizeBestFoodSource();
	}
	
	public ArtificialBeeColony(ImageModel img, int colonySize, int limit, double mr, double sf) {
		this(img, colonySize, limit);
		this.mr = mr;
		this.sf = sf;
	}

	private void initializeFoodSources() {
		for (int i = 0; i < foodNumber; i++) {
			initializeFoodSource(i);
		}
	}

	private void initializeFoodSource(int i) {
		for (int j = 0; j < parameterSize; j++) {
			//foods[i][j] = Randomizer.randomizeInt(lowerBound[j], upperBound[j]);
			foods[i][j] = Randomizer.randomizeInt(0, 255);
		}
		ofValues[i] = calculateObjectiveFunctionValue(foods[i]);
		fitnessValues[i] = calculateFitnessValue(ofValues[i]);
		trial[i] = 0;
	}
	
	private void reInitializeFoodSource(int i) {
		for (int j = 0; j < parameterSize; j++) {
			foods[i][j] = Randomizer.randomizeInt(lowerBound[j], upperBound[j]);
			//foods[i][j] = Randomizer.randomizeInt(0, 255);
		}
		ofValues[i] = calculateObjectiveFunctionValue(foods[i]);
		fitnessValues[i] = calculateFitnessValue(ofValues[i]);
		trial[i] = 0;
	}
	
	/**
	 * todo: add code that simulates the basic ABC when
	 * 		 modification rate = 1 and scaling factor = 1
	 */
	private void neighborhoodSearch(int i) {
		int neighborIndex = Randomizer.randomizeInt(0, foodNumber - 1);
		int newFoodSource[] = new int[parameterSize];
		double phi = Randomizer.randomizeDouble(0 - sf, 0 + sf);
		
		while ((neighborIndex = Randomizer.randomizeInt(0, foodNumber - 1)) == i);

		for (int j = 0; j < newFoodSource.length; j++) {
			double r = Randomizer.randomizeDouble(0, 1);
			if (r < this.mr) {
				int temp =  (foods[i][j] + (int) (phi * (foods[i][j] - foods[neighborIndex][j])));
				temp = temp < lowerBound[j] ? foods[i][j] : temp;
				temp = temp > upperBound[j] ? foods[i][j] : temp;
				newFoodSource[j] = temp;
			}
			else {
				newFoodSource[j] = foods[i][j];
			}
		}

		double newObjVal = calculateObjectiveFunctionValue(newFoodSource);
		double newFitVal = calculateFitnessValue(newObjVal);

		if (newFitVal > fitnessValues[i]) {
			for (int j = 0; j < newFoodSource.length; j++) {
				foods[i][j] = newFoodSource[j];
			}
			ofValues[i] = newObjVal;
			fitnessValues[i] = newFitVal;
			trial[i] = 0;
		} else {
			trial[i] = trial[i] + 1;
		}
	}

	private void calculateProbabilities() {
		double totalFitness = 0;
		for (int i = 0; i < foodNumber; i++) {
			totalFitness += fitnessValues[i];
		}
		for (int i = 0; i < foodNumber; i++) {
			probs[i] = fitnessValues[i] / totalFitness;
		}
	}

	private double calculateObjectiveFunctionValue(int[] sol) {
		return -img.getPSNR(sol);
	}

	private double calculateFitnessValue(double i) {
		if (i >= 0) {
			return 1.0 / (1.0 + i);
		} else {
			return 1 + Math.abs(i);
		}
	}

	public void memorizeBestFoodSource() {
		for (int i = 0; i < foodNumber; i++) {
			if (fitnessValues[i] > bestSolutionFitnessValue) {
				bestSolutionFitnessValue = fitnessValues[i];
				bestSolutionValue = ofValues[i];
				bestSolutionIndex = i;
				for (int j = 0; j < parameterSize; j++) {
					bestSolution[j] = foods[i][j];
				}
			}
		}
	}

	public void sendEmployedBees() {
		for (int i = 0; i < foodNumber; i++) {
			neighborhoodSearch(i);
		}
		calculateProbabilities();
	}

	public void sendOnlookerBees() {
		for (int i = 0, j = 0; i < foodNumber;) {
			if (Randomizer.randomizeZeroToOne() < probs[j]) {
				neighborhoodSearch(j);
				i++;
			}
			j = (j == foodNumber - 1) ? 0 : j + 1;
		}
		memorizeBestFoodSource();
	}

	public void sendScoutBees() {
		int maxTrialIndex = 0;
		for (int i = 0; i < foodNumber; i++) {
			if (trial[i] > trial[maxTrialIndex]) {
				maxTrialIndex = i;
			}
		}
		//if (trial[maxTrialIndex] >= limit && maxTrialIndex != bestSolutionIndex) {
		if (trial[maxTrialIndex] >= limit) {
			reInitializeFoodSource(maxTrialIndex);
			//initializeFoodSource(maxTrialIndex);
		}
		memorizeBestFoodSource();
	}

	public double getBestSolutionValue() {
		return -bestSolutionValue;
	}

	public int[] getBestSolution() {
		return bestSolution;
	}
}
